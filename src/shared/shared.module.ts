import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    SharedRoutingModule
  ],
  exports:[
    HeaderComponent,
    FooterComponent

  ],
  providers: [],
})
export class SharedModule { }
