import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FormService {
  form_url = 'http://localhost:3000/form';
  constructor(
    private http: HttpClient
    ) { }
    
  public postForm(form: any){
    let headers = { 'Authorization': ''}
    return this.http.post(this.form_url,form,{headers:headers});
  }
}
