import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormService } from 'src/services/form.service';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.sass']
})

export class ContatoComponent implements OnInit {
  x: FormGroup = this.formBuilder.group({});
  constructor(
    private formBuilder: FormBuilder,
    private FormSvc: FormService
    ){}
  ngOnInit(): void {
    this.x = this.formBuilder.group({
      nome: [''],
      email: [''],
      assunto: [''],
      mensagem: ['']
    });
  }

  submitForm(){
    //Aqui ele pega os dados do formulario num array, igual ao que o form.value faz
    console.log(this.x.value);
    this.FormSvc.postForm(this.x.value).subscribe(data=>{
      console.log(data);
    });
  }

}
