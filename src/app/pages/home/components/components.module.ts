import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SliderComponent } from './slider/slider.component';
import { SobreComponent } from './sobre/sobre.component';
import { EquipeComponent } from './equipe/equipe.component';
import { EspacoComponent } from './espaco/espaco.component';
import { ContatoComponent } from './contato/contato.component';



@NgModule({
  declarations: [
    SliderComponent,
    SobreComponent,
    EquipeComponent,
    EspacoComponent,
    ContatoComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [
    SliderComponent,
    SobreComponent,
    EquipeComponent,
    EspacoComponent,
    ContatoComponent
  ],
})
export class ComponentsModule { }
