import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from 'src/shared/shared.module';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from './home/components/components.module';
import { FormService } from 'src/services/form.service';

@NgModule({
  declarations: [
  
    HomeComponent
  ],
  imports: [
    BrowserModule,
    PagesRoutingModule,
    SharedModule,
    ComponentsModule
  ],
  providers: [
    SharedModule,
    FormService
  ],
  bootstrap: [
  ],
  exports:[
    HomeComponent
  ]
})
export class PagesModule { }
